const express = require('express')
const router = express.Router()

const mongoose = require('../database/mongoose');

const Student = require('../models/student')
const Subject = require('../models/subject')


router.get('/', (req,res) => {
    res.send('From API route')
})

// Retrieve Students data
router.get('/students',(req,res) => {
    Student.find({})
    .then((students) => res.send(students))
    .catch((error) => console.log(error));
})

//send student data
router.post('/students',(req,res) => {
    (new Student({'name': req.body.name })).save()
    .then((student) => res.send(student))
    .catch((error) => console.log(error));
})

// Retrieve single Student data
router.get('/students/:stId',(req,res) => {
    Student.find({_id:req.params.stId})
    .then((student) => res.send(student))
    .catch((error) => console.log(error));
})

//Update Single student
router.patch('/students/:stId',(req,res) => {
    Student.findOneAndUpdate({_id : req.params.stId } , { $set : req.body} )
    .then((student) => res.send(student))
    .catch((error) => console.log(error));
})

//Delete Single student
router.delete('/students/:stId',(req,res) => {
    const deleteSubjects = student => {
        Subject.deleteMany({_stuId:student._id})
        .then(() => student)
        .catch((error) => console.log(error));
    }
    Student.findByIdAndDelete(req.params.stId)
    .then((student) => res.send(deleteSubjects(student)))
    .catch((error) => console.log(error));
})

router.get('/students/:stId/subjects' , (req,res) =>{
    Subject.find({_stuId:req.params.stId})
    .then((subjects) => res.send(subjects))
    .catch((error) => console.log(error));
})

router.post('/students/:stId/subjects' , (req,res) => {
    (new Subject({'subjectName':req.body.subjectName , 'marks':req.body.marks , '_stuId':req.params.stId}))
    .save()
    .then((subjects) => res.send(subjects))
    .catch((error) => console.log(error));
})

router.get('/students/:stId/subjects/:subId' , (req,res) =>{
    Subject.findOne({_stuId:req.params.stId , _id:req.params.subId})
    .then((subject) => res.send(subject))
    .catch((error) => console.log(error));
})

router.patch('/students/:stId/subjects/:subId' , (req,res) =>{
    Subject.findOneAndUpdate({_stuId:req.params.stId , _id:req.params.subId},{$set:req.body})
    .then((subject) => res.send(subject))
    .catch((error) => console.log(error));
})


router.delete('/students/:stId/subjects/:subId' , (req,res) =>{
    Subject.findByIdAndDelete({_stuId:req.params.stId , _id:req.params.subId})
    .then((subject) => res.send(subject))
    .catch((error) => console.log(error));
})


module.exports = router