const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()

app.use(cors())

const api = require('./routes/api')

const mongoose = require('./database/mongoose');

const Student = require('./models/student')
const Subject = require('./models/subject')

const PORT = 3000

app.use(express.json());
app.use('/api',api)

app.listen(PORT,function(){
    console.log('Server running on localhost: '+PORT);
})