const mongoose = require('mongoose')

const Schema = mongoose.Schema

const subjectSchema = new Schema({
    subjectName: String,
    marks: String,
    _stuId:{
        type: mongoose.Types.ObjectId,
        required:true
    }
})

module.exports = mongoose.model('subject',subjectSchema,'subject')