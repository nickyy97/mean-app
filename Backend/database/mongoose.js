const mongoose = require('mongoose')

const db = "mongodb+srv://admin:nickdex11@cluster0-txpt4.mongodb.net/test?retryWrites=true&w=majority"

mongoose.connect(db, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify:false
    }
    ,err =>{
        if(err){
            console.error('Error' + err)
        }else{
            console.log('Connected to mongodb!!!')
    }
})

module.exports = mongoose;