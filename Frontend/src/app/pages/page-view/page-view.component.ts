import { Component, OnInit } from '@angular/core';
import Student from 'src/app/models/student';
import Subject from 'src/app/models/subject';
import { StudentService } from 'src/app/service/student.service';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-page-view',
  templateUrl: './page-view.component.html',
  styleUrls: ['./page-view.component.css']
})
export class PageViewComponent implements OnInit {

  student:Student[] = [];
  subject:Subject[] = [];

  constructor(private stuService:StudentService , private activeRoute:ActivatedRoute , private route:Router) { }

  ngOnInit(){

    this.stuService.getStudents().subscribe(
      (students:Student[]) => this.student = students
    );

    this.activeRoute.params.subscribe((params:Params) => {
      const stuId = params.stuId;
      if(!stuId) return;
      this.stuService.getSubjects(stuId).subscribe(
        (subjects:Subject[]) => this.subject = subjects
      );
    })

  }

}
