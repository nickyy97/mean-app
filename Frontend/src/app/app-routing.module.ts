import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageViewComponent } from './pages/page-view/page-view.component';


const routes: Routes = [
  { path: '',    redirectTo: '/students', pathMatch: 'full'  },
  {path: 'students' , component: PageViewComponent},
  {path: 'students/:stuId' , component: PageViewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
