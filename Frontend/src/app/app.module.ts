import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageViewComponent } from './pages/page-view/page-view.component';
import { from } from 'rxjs';
import { AddStudentComponent } from './pages/add-student/add-student.component';

@NgModule({
  declarations: [
    AppComponent,
    PageViewComponent,
    AddStudentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
