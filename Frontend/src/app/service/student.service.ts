import { Injectable } from '@angular/core';
import { WebService } from './web.service';
import Student from '../models/student';
import  Subject  from '../models/subject';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private webService:WebService) { }

  getStudents(){
    return this.webService.get('students');
  }

  addStudent(name:string){
    return this.webService.post('students', {name} );
  }

  deleteStudent(stuId: string){
    return this.webService.delete(`student/${stuId}`)
  }

  // updateStudent(stuId: string, student:Student){
  //   return this.webService.patch(`student/${stuId}`, {name})
  // }

  // vbnv


  getSubjects(stuId: string){
    return this.webService.get(`students/${stuId}/subjects`);
  }

  addSubject(stuId: string, name:string){
    return this.webService.post(`students/${stuId}/subjects`, {name} );
  }

  deleteSubject(stuId: string, subId:string){
    return this.webService.delete(`students/${stuId}/subjects/${subId}`)
  }

  // updateSubject(stuId: string,  subId:string, subject:Subject){
  //   return this.webService.patch(`students/${stuId}/subjects/${subject._id}`, {name})
  // }

}
